## Задание:
1. Установите PostgreSQL v13 на предоставленной VM c OS Ubuntu 20.04.
2. Добавьте в PostgreSQL пользователя root c правами SUPERUSER.
3. Подключитесь к PostgreSQL пользователем root.
4. Создайте базу данных "rebrain_courses_db".
5. Для работы с базой данных создайте пользователя "rebrain_admin".
6. Выдайте все права пользователю "rebrain_admin" на базу данных "rebrain_courses_db" (в том числе CONNECT).
7. Скачайте и установите prometheus-server, проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/9090 (ss, netstat). Также настройте сбор данных о самом сервере [prometheus selfmonitoring] (job_name: 'prometheus').
8. Скачайте и установите в режиме сервиса последнюю версию node_exporter для OS Ubuntu 20.04 Focal, проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/9100 (ss, netstat).
9. Настройте новый источник данных для prometheus-server, подключите сбор данных с node_exporter (job_name: 'node_exporter').
10. Скачайте последнюю версию Grafana для OS Ubuntu 20.04 Focal, установите ее в режиме сервиса, проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/3000 (ss, netstat).
11. Подключите к Grafana сервер prometheus в качестве источника данных для панелей мониторинга.
12. Скачайте и импортируйте Dashboard 1860 для node_exporter. Выберите в качестве источника данных prometheus сервер.
13. Проверьте работоспособность node_exporter воспользовавшись импортированной панелью мониторинга 1860. Все метрики должны быть видны на графиках и в полях значений.
14. Скачайте и установите в качестве сервиса самую свежую версию postgres_exporter (можно сразу ставить через `apt install prometheus-postgres-exporter`), проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/9187 (ss, netstat). Подключите сбор данных с prometheus-postgres-exporter (job_name: 'postgres_exporter').
15. Добавьте в PostgreSQL пользователя rebrain_monitoring c правами SUPERUSER.
16. Настройте окружение для postgres_exporter так, чтобы работало подключение к PostgreSQL к базе данных rebrain_courses_db с правами rebrain_monitoring. Для этого используйте файл "/etc/default/prometheus-postgres-exporter", в него в одну строку внесите данные о подключении (DATA_SOURCE_NAME).
17. Скачайте и импортируйте Dashboard 9628 для postgres_exporter и мониторинга PostgreSQL.
Выберите в качестве источника данных prometheus-server.
18. Проверьте работоспособность postgres_exporter воспользовавшись импортированной панелью мониторинга 9628. Все метрики должны быть видны на графиках и в полях значений.
19. Проинициализируйте pgbench для базы данных rebrain_courses_db (если это необходимо, поставьте пакет postgresql-client-common).
20. Проведите нагрузочный тест с помощью pgbench (50 конкурентных клиентов в 5 потоков, 120 секунд). Метрики для PostgreSQL будут изменяться с течением времени.
21. Проанализируйте графики на обеих панелях мониторинга, убедитесть, что нагрузочный тест проведен успешно.
22. Если уверены, что все сделали правильно, сдавайте задание на проверку.

## Ответы
1. PostgreSQL v13 c OS Ubuntu 20.04
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql-13 net-tools mc 
```
2 - 6. 
```
sudo su - postgres
CREATE USER root WITH LOGIN SUPERUSER PASSWORD 'root';
psql -U root -h localhost -d postgres

CREATE DATABASE rebrain_courses_db;
CREATE USER rebrain_admin WITH LOGIN PASSWORD 'rebrain_admin';
GRANT ALL PRIVILEGES ON DATABASE rebrain_courses_db to rebrain_admin;
```
7. Скачайте и установите prometheus-server, проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/9090 (ss, netstat). Также настройте сбор данных о самом сервере [prometheus selfmonitoring] (job_name: 'prometheus').
```
cd /opt/ &&
sudo wget https://github.com/prometheus/prometheus/releases/download/v2.26.0/prometheus-2.26.0.linux-amd64.tar.gz

sudo tar zxf prometheus-2.26.0.linux-amd64.tar.gz &&
sudo mv prometheus-2.26.0.linux-amd64 prometheus &&
sudo rm -rf prometheus-2.26.0.linux-amd64.tar.gz
```

```
sudo vi /etc/systemd/system/prometheus.service

[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
ExecStart=/opt/prometheus/prometheus \
    --config.file=/opt/prometheus/prometheus.yml \
    --storage.tsdb.path=/opt/prometheus/data \
    --web.console.templates=/opt/prometheus/consoles \
    --web.console.libraries=/opt/prometheus/console_libraries \
    --web.listen-address="0.0.0.0:9090"

[Install]
WantedBy=default.target
```

```
sudo useradd -s /sbin/nologin -d /usr/local/bin/ prometheus 

# и не забываем про назчание прав для нужных директории (смотри выше)
sudo chown prometheus:prometheus -R /opt/prometheus
```

```
sudo systemctl daemon-reload &&
sudo systemctl enable prometheus &&
sudo systemctl start prometheus &&
sudo systemctl status prometheus 
```
8. Скачайте и установите в режиме сервиса последнюю версию node_exporter для OS Ubuntu 20.04 Focal, проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/9100 (ss, netstat).
9. Настройте новый источник данных для prometheus-server, подключите сбор данных с node_exporter (job_name: 'node_exporter').
```
cd /opt/ &&
sudo wget https://github.com/prometheus/node_exporter/releases/download/v1.1.2/node_exporter-1.1.2.linux-amd64.tar.gz

sudo tar zxf node_exporter-1.1.2.linux-amd64.tar.gz &&
sudo mv node_exporter-1.1.2.linux-amd64 node_exporter && 
sudo rm -rf node_exporter-1.1.2.linux-amd64.tar.gz
```

```
sudo vi /etc/systemd/system/node_exporter.service

[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
ExecStart=/opt/node_exporter/node_exporter --web.listen-address="0.0.0.0:9100" --collector.interrupts

[Install]
WantedBy=multi-user.target
```

```
sudo chown prometheus:prometheus -R /opt/node_exporter

sudo systemctl daemon-reload &&
sudo systemctl enable node_exporter &&
sudo systemctl start node_exporter &&
sudo systemctl status node_exporter
```

Добавляем сбор метрики экспортера node_exporter

```
sudo vi /opt/prometheus/prometheus.yml
```

```
scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['localhost:9090']

  - job_name: 'node_exporter'
    static_configs:
      - targets: ['localhost:9100']
```

10. Скачайте последнюю версию Grafana для OS Ubuntu 20.04 Focal, установите ее в режиме сервиса, проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/3000 (ss, netstat).
```
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
sudo apt update
sudo apt install grafana -y
sudo systemctl start grafana-server
sudo systemctl status grafana-server
sudo systemctl enable grafana-server
```
14. Скачайте и установите в качестве сервиса самую свежую версию postgres_exporter (можно сразу ставить через `apt install prometheus-postgres-exporter`), проверьте доступность сервиса (systemctl), посмотрите, открыт ли порт TCP/9187 (ss, netstat). Подключите сбор данных с prometheus-postgres-exporter (job_name: 'postgres_exporter').
```
sudo apt install prometheus-postgres-exporter 

sudo vi /opt/prometheus/prometheus.yml

scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['localhost:9090']

  - job_name: 'node_exporter'
    static_configs:
      - targets: ['localhost:9100']

  - job_name: 'postgres_exporter'
    static_configs:
      - targets: ['localhost:9187']

sudo systemctl restart prometheus
```
15. Добавьте в PostgreSQL пользователя rebrain_monitoring c правами SUPERUSER.
```
CREATE USER rebrain_monitoring WITH SUPERUSER LOGIN;

GRANT ALL PRIVILEGES ON DATABASE rebrain_courses_db to rebrain_monitoring;
```
16. Настройте окружение для postgres_exporter так, чтобы работало подключение к PostgreSQL к базе данных rebrain_courses_db с правами rebrain_monitoring. Для этого используйте файл "/etc/default/prometheus-postgres-exporter", в него в одну строку внесите данные о подключении (DATA_SOURCE_NAME).
```
sudo vi /etc/default/prometheus-postgres-exporter 

DATA_SOURCE_NAME='user=rebrain_monitoring host=/run/postgresql dbname=rebrain_courses_db' 
```
17. Скачайте и импортируйте Dashboard 9628 для postgres_exporter и мониторинга PostgreSQL.
Выберите в качестве источника данных prometheus-server.
18. Проверьте работоспособность postgres_exporter воспользовавшись импортированной панелью мониторинга 9628. Все метрики должны быть видны на графиках и в полях значений.
19. Проинициализируйте pgbench для базы данных rebrain_courses_db (если это необходимо, поставьте пакет postgresql-client-common).
```
sudo apt install postgresql-client-common
```
20. Проведите нагрузочный тест с помощью pgbench (50 конкурентных клиентов в 5 потоков, 120 секунд). Метрики для PostgreSQL будут изменяться с течением времени.
21. Проанализируйте графики на обеих панелях мониторинга, убедитесть, что нагрузочный тест проведен успешно.
22. Если уверены, что все сделали правильно, сдавайте задание на проверку.
